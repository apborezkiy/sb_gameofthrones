package ui.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import data.managers.DataManager;
import data.storage.models.CharacterModel;
import data.storage.models.HouseModel;
import mvp.presenters.IMainPresenter;
import mvp.presenters.MainPresenter;
import mvp.views.IMainView;
import utils.ConstantManager;

import com.softdesign.sb0_bap.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IMainView {
    private static String TAG = "Main Activity";

    private DataManager mDataManager;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @BindView(R.id.container) ViewPager mViewPager;
    @BindView(R.id.navigation_view) NavigationView mNavigationView;
    @BindView(R.id.navigation_container) DrawerLayout mDrawerLayout;
    @BindView(R.id.pager_tab_strip) PagerTabStrip mPagerTabStrip;

    //region ======= life cycle =======

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mMainPresenter = MainPresenter.getInstance();
        mDataManager = DataManager.getInstance();

        ListView lvHouse = (ListView) findViewById(R.id.lvHouses);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, mDataManager.getHousesNamesLocal());
        lvHouse.setAdapter(adapter);
        lvHouse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPagerTabStrip.scrollTo(i, mPagerTabStrip.getScrollY());
                mViewPager.setCurrentItem(i);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Game of Thrones");
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        ab.setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    //endregion

    //region ======= user events =======

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            mDrawerLayout.openDrawer(GravityCompat.START);

        return super.onOptionsItemSelected(item);
    }

    //endregion

    //region ======= tabs =======

    // TODO: 26.10.16 Все-таки желательно выполнять декомпозицию по классам, чтобы не запихивать все в один

    public static class PlaceholderFragment extends Fragment implements ItemClickListener {
        @Override
        public void OnItemClicked(Integer position) {
            Log.d(TAG, "Next screen");
            CharacterModel character = ((RecyclerViewAdapter) mRecyclerView.getAdapter()).records.get(position);
            Intent intent = new Intent(getContext(), InfoActivity.class);
            intent.putExtra(ConstantManager.CHARACTER_REMOTE_ID, character.remoteId);
            intent.putExtra(ConstantManager.HOUSE_REMOTE_ID, DataManager.getInstance().getHousesLocal().get(section_number - 1).remoteId);
            startActivity(intent);
        }

        private static final String ARG_SECTION_NUMBER = "section_number";
        private Integer section_number;
        private RecyclerView mRecyclerView;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            this.section_number = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.character_list);
            Long remoteId = DataManager.getInstance().getHousesLocal().get(section_number - 1).remoteId;
            mRecyclerView.setAdapter(new RecyclerViewAdapter(DataManager.getInstance().getCharacters(remoteId), this));
            mRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
            return rootView;
        }

        //region ======= recycle view =======

        public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

            public List<CharacterModel> records;
            private ItemClickListener listener;

            public RecyclerViewAdapter(List<CharacterModel> records, ItemClickListener listener) {
                this.records = records;
                this.listener = listener;
            }

            @Override
            public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.character_list_item, viewGroup, false);
                return new ViewHolder(v, listener);
            }

            @Override
            public void onBindViewHolder(ViewHolder viewHolder, int i) {
                CharacterModel record = records.get(i);
                viewHolder.name.setText(record.name);
                viewHolder.info.setText("Mother " + (record.getFather().length() > 0 ? record.getFather() : "unknown") + ", Father " + (record.getMother().length() > 0 ? record.getMother() : "unknown"));
            }

            @Override
            public int getItemCount() {
                return records.size();
            }

            class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
                private TextView name;
                private TextView info;
                private ImageView icon;
                private ItemClickListener listener;

                @Override
                public void onClick(View v) {
                    listener.OnItemClicked(getAdapterPosition());
                }

                public ViewHolder(View itemView, ItemClickListener listener) {
                    super(itemView);
                    itemView.setOnClickListener(this);
                    this.listener = listener;
                    name = (TextView) itemView.findViewById(R.id.clist_text_view_name);
                    info = (TextView) itemView.findViewById(R.id.clist_text_view_info);
                    icon = (ImageView) itemView.findViewById(R.id.clist_image_view);
                }
            }
        }

        //endregion
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return mDataManager.getHousesLocal().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mDataManager.getHousesLocal().get(position).name;
        }
    }

    public interface ItemClickListener {
        void OnItemClicked (Integer position);
    }

    //endregion ======= =======

    //region ======= IView =======

    MainPresenter mMainPresenter;

    @Override
    public IMainPresenter getPresenter() {
        return mMainPresenter;
    }

    @Override
    public void showMessage(String message) {

    }

    //endregion
}
