package ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.Snackbar;

import com.softdesign.sb0_bap.R;

import java.io.IOException;
import java.util.List;

import data.managers.DataManager;
import data.network.req.HouseReq;
import data.network.res.CharacterRes;
import data.network.res.HouseRes;
import mvp.presenters.ISplashPresenter;
import mvp.presenters.SplashPresenter;
import mvp.views.ISplashView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.NetworkStatusChecker;

public class SplashActivity extends AppCompatActivity implements ISplashView {
    final static String TAG = "SplashActivity";

    private DataManager mDataManager;
    private Context mContext = this;

    private ProgressDialog mProgressDialog;
    private View mSplashFrame;
    private static Boolean sFirstLoad = true;

    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;

    private final Handler mHideHandler = new Handler();
    private final Handler mLoadingHandler = new Handler();

    private View mContentView;
    private View mControlsView;
    private boolean mVisible;

    //region ======= life cycle =======

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mISplashPresenter = SplashPresenter.getInstance();

        setContentView(R.layout.activity_splash);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu_black_24dp));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Game of thrones");

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);
        mSplashFrame = findViewById(R.id.splash_frame);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);

        // load data
        if (sFirstLoad == true) {
            sFirstLoad = false;
            LoaderTask task = new LoaderTask();
            task.execute();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProgressDialog.dismiss();
    }

    //endregion

    //region ======= full screen =======

    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };

    //endregion

    //region ======= spinner =======

    protected void showProgress (Context context) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context, R.style.progress_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_dialog);
        }
    }

    protected void hideProgress () {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    //endregion

    //region ======= data loading task =======

    // TODO: 26.10.16 AsyncTask - не самый удачный выбор в 2016 году =)
    class LoaderTask extends AsyncTask<Void, Void, Integer> {

        private Long mTimestampStarted;
        private DataManager mDataManager = DataManager.getInstance();
        private Integer mCode = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mTimestampStarted = System.currentTimeMillis();
            showProgress(mContext);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                mDataManager.loadFromDatabaseOrRemoteService();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DataManager.CodeException e) {
                mCode = e.code();
            }
            return mCode;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 0)
                nextActivity(mTimestampStarted);
            else {
                if (result == 403) {
                    Snackbar.make(mSplashFrame, "Требуется авторизация", Snackbar.LENGTH_LONG).show();
                } else if (result == -1) {
                    Snackbar.make(mSplashFrame, "Сеть недоступна", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(mSplashFrame, "Ошибка " + Integer.toString(result) + " при загрузке", Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    //endregion

    //region ======= transitions =======

    private void nextActivity (Long timestampStarted) {
        final Context context = this;

        mLoadingHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }, Math.max(3000 - (System.currentTimeMillis() - timestampStarted), 0));
    }

    //endregion

    //region ======= IView =======

    private ISplashPresenter mISplashPresenter;

    @Override
    public ISplashPresenter getPresenter() {
        return mISplashPresenter;
    }

    @Override
    public void showMessage(String message) {

    }

    //endregion
}
