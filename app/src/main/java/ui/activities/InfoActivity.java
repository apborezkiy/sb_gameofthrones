package ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.sb0_bap.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import data.managers.DataManager;
import data.storage.models.CharacterModel;
import data.storage.models.HouseModel;
import mvp.presenters.CharacterPresenter;
import mvp.presenters.ICharacterPresenter;
import mvp.views.ICharacterView;
import utils.ConstantManager;

public class InfoActivity extends AppCompatActivity implements ICharacterView, ConstantManager {
  private static final String TAG = "Info Activity";

  private Context mContext;
  private Long characterRemoteId;
  private Long houseRemoteId;
  private CharacterModel mCharacter;
  private HouseModel mHouse;

  @BindView(R.id.house_icon) ImageView mImageView;
  @BindView(R.id.father_button) Button mFatherButton;
  @BindView(R.id.mother_button) Button mMotherButton;
  @BindView(R.id.main_content) CoordinatorLayout mCoordinatorLayout;

  //region ======= life cycle =======

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);
    ButterKnife.bind(this);

    mCharacterPresenter = new CharacterPresenter();
    mContext = this;

    // extract params
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      characterRemoteId = extras.getLong(ConstantManager.CHARACTER_REMOTE_ID);
      houseRemoteId = extras.getLong(ConstantManager.HOUSE_REMOTE_ID);
      mCharacter = DataManager.getInstance().getCharacterLocal(characterRemoteId);
      mHouse = DataManager.getInstance().getHouseLocal(houseRemoteId);
      Log.d(TAG, "house info " + mHouse.remoteId.toString());
      Log.d(TAG, "character info " + mCharacter.remoteId.toString());
    }

    // setup toolbar
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setTitle(mCharacter.getName());
    setSupportActionBar(toolbar);
    ActionBar ab = getSupportActionBar();
    ab.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
    ab.setDisplayHomeAsUpEnabled(true);

    // init text fields
    Log.d(TAG, "house info " + mHouse.getName());
    ((TextView) findViewById(R.id.words_text)).setText(mHouse.getWords().length() > 0 ? mHouse.getWords() : "Unknown");
    ((TextView) findViewById(R.id.words_born)).setText(mCharacter.getBorn().length() > 0 ? mCharacter.getBorn() : "Unknown");
    ((TextView) findViewById(R.id.words_titles)).setText(mCharacter.getTitles().length() > 2 ? mCharacter.getTitles().replaceAll("([\\[\\]])", "").replaceAll(", ", "\n") : "Unknown\n...\n...\n...\n...\n...");
    ((TextView) findViewById(R.id.words_aliases)).setText(mCharacter.getAliases().length() > 2 ? mCharacter.getAliases().replaceAll("([\\[\\]])", "").replaceAll(", ", "\n") : "Unknown\n...\n...\n...\n...\n...\n" +
            "...\n" +
            "...\n" +
            "...\n" +
            "...\n" +
            "...");

    mFatherButton.setText(mCharacter.getFather().length() > 0 ? mCharacter.getFather() : "UNKNOWN");
    mMotherButton.setText(mCharacter.getMother().length() > 0 ? mCharacter.getMother() : "UNKNOWN");

    // setup onclick listeners
    Button.OnClickListener l = new Button.OnClickListener() {
      @Override
      public void onClick(View view) {
        CharacterModel parent = null;
        for (CharacterModel ch : DataManager.getInstance().getCharacters(houseRemoteId)) {
          if (ch.getName() == ((Button) view).getText()) {
            Intent i = new Intent(mContext, InfoActivity.class);
            i.putExtra(ConstantManager.HOUSE_REMOTE_ID, houseRemoteId);
            i.putExtra(ConstantManager.CHARACTER_REMOTE_ID, ch.getRemoteId());
            startActivity(i);
            return;
          }
        }

        Snackbar.make(mCoordinatorLayout, "No character found", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
      }
    };

    mFatherButton.setOnClickListener(l);
    mMotherButton.setOnClickListener(l);

    // show extra info
    if (mCharacter.getDied().length() > 0) {
      String[] seasons = mCharacter.getTvSeries().replaceAll("([\\[\\]])", "").replaceAll(", ", "\n").split("\n");

      if (seasons.length > 0) {
        String season = seasons[seasons.length - 1];
        Snackbar.make(mCoordinatorLayout, "Died " + season, Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    }
  }

  //endregion

  //region ======= user events =======

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  //endregion

  //region ======= IView =======

  CharacterPresenter mCharacterPresenter;

  @Override
  public ICharacterPresenter getPresenter() {
    return mCharacterPresenter;
  }

  @Override
  public void showMessage(String message) {

  }

  //endregion
}
