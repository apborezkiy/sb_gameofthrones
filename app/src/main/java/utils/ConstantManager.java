package utils;

public interface ConstantManager {
  String IS_FIRST_LAUNCH = "IS_FIRST_LAUNCH";
  String CHARACTER_REMOTE_ID = "CHARACTER_REMOTE_ID";
  String HOUSE_REMOTE_ID = "HOUSE_REMOTE_ID";
}
