package utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.stetho.Stetho;

import data.storage.models.DaoMaster;
import data.storage.models.DaoSession;
import org.greenrobot.greendao.database.Database;

/**
 * Created by apborezkiy on 15.10.16.
 */

public class ThisApplication extends Application {

    private static SharedPreferences sSharedPreferences;
    private static Context sContext;

    private static DaoSession sDaoSession;

    public static Context getContext () {
        return sContext;
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sContext = getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "SB0-BAP-DB");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }
}
