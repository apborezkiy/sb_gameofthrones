package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.models.SplashModel;
import mvp.views.ISplashView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public class SplashPresenter implements ISplashPresenter {
    private static SplashPresenter ourInstance = new SplashPresenter();
    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    private ISplashView mIView;
    private SplashModel mModel;

    public SplashPresenter() {
        mModel = new SplashModel();
    }

    @Override
    public void takeView(ISplashView splashView) {
        mIView = splashView;
    }

    @Override
    public void dropView() {
        mIView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {

        }
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mIView;
    }
}
