package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.views.ISplashView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface ISplashPresenter {
    void takeView(ISplashView authView);
    void dropView();
    void initView();

    @Nullable
    ISplashView getView();
}
