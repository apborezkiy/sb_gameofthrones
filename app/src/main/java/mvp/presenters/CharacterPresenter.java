package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.models.CharacterModel;
import mvp.views.ICharacterView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public class CharacterPresenter implements ICharacterPresenter {
    private static CharacterPresenter ourInstance = new CharacterPresenter();
    public static CharacterPresenter getInstance() {
        return ourInstance;
    }

    private ICharacterView mIView;
    private CharacterModel mModel;

    public CharacterPresenter() {
        mModel = new CharacterModel();
    }

    @Override
    public void takeView(ICharacterView characterView) {
        mIView = characterView;
    }

    @Override
    public void dropView() {
        mIView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {

        }
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mIView;
    }
}
