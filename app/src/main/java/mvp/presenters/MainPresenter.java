package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.models.MainModel;
import mvp.views.IMainView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public class MainPresenter implements IMainPresenter {
    private static MainPresenter ourInstance = new MainPresenter();
    public static MainPresenter getInstance() {
        return ourInstance;
    }

    private IMainView mIView;
    private MainModel mModel;

    public MainPresenter() {
        mModel = new MainModel();
    }

    @Override
    public void takeView(IMainView mainView) {
        mIView = mainView;
    }

    @Override
    public void dropView() {
        mIView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {

        }
    }

    @Nullable
    @Override
    public IMainView getView() {
        return mIView;
    }
}
