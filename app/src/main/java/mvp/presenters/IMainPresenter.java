package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.views.IMainView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface IMainPresenter {
    void takeView(IMainView authView);
    void dropView();
    void initView();

    @Nullable
    IMainView getView();
}
