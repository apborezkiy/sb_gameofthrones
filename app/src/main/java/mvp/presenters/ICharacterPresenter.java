package mvp.presenters;

import android.support.annotation.Nullable;

import mvp.views.ICharacterView;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface ICharacterPresenter {
    void takeView(ICharacterView authView);
    void dropView();
    void initView();

    @Nullable
    ICharacterView getView();
}
