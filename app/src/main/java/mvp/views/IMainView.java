package mvp.views;

import android.support.annotation.Nullable;

import mvp.presenters.IMainPresenter;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface IMainView {
    IMainPresenter getPresenter();

    void showMessage(String message);
}
