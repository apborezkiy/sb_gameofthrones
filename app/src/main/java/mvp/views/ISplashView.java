package mvp.views;

import android.support.annotation.Nullable;

import mvp.presenters.IMainPresenter;
import mvp.presenters.ISplashPresenter;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface ISplashView {
    ISplashPresenter getPresenter();

    void showMessage(String message);
}
