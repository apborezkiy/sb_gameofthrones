package mvp.views;

import android.support.annotation.Nullable;

import mvp.presenters.ICharacterPresenter;

/**
 * Created by apborezkiy on 26.10.16.
 */

public interface ICharacterView {
    ICharacterPresenter getPresenter();

    void showMessage(String message);
}
