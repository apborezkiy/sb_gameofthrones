package data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import data.network.res.HouseRes;

@Entity(active = true, nameInDb = "HOUSES")
public class HouseModel {

  @Id
  private Long id;

  @Unique
  @NotNull
  public Long remoteId;

  @Unique
  @NotNull
  public String url;

  public String words;

  @NotNull
  public String name;

  @ToMany(joinProperties = {
      @JoinProperty(name = "remoteId", referencedName = "houseId")
  })
  public List<CharacterModel> swornMembers;

  /** Used for active entity operations. */
  @Generated(hash = 566542349)
  private transient HouseModelDao myDao;

  /** Used to resolve relations */
  @Generated(hash = 2040040024)
  private transient DaoSession daoSession;

  public HouseModel(Long remoteId, HouseRes house) {
    this.remoteId = remoteId;
    this.url = house.url;
    this.name = house.name;
    this.words = house.words;
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 1942392019)
  public void refresh() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 713229351)
  public void update() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 128553479)
  public void delete() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
  }

  /** Resets a to-many relationship, making the next get call to query for a fresh result. */
  @Generated(hash = 264415194)
  public synchronized void resetSwornMembers() {
    swornMembers = null;
  }

  /**
   * To-many relationship, resolved on first access (and after reset).
   * Changes to to-many relations are not persisted, make changes to the target entity.
   */
  @Generated(hash = 877811569)
  public List<CharacterModel> getSwornMembers() {
      if (swornMembers == null) {
          final DaoSession daoSession = this.daoSession;
          if (daoSession == null) {
              throw new DaoException("Entity is detached from DAO context");
          }
          CharacterModelDao targetDao = daoSession.getCharacterModelDao();
          List<CharacterModel> swornMembersNew = targetDao._queryHouseModel_SwornMembers(remoteId);
          synchronized (this) {
              if(swornMembers == null) {
                  swornMembers = swornMembersNew;
              }
          }
      }
      return swornMembers;
  }

  /** called by internal mechanisms, do not call yourself. */
  @Generated(hash = 2044046615)
  public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getHouseModelDao() : null;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getWords() {
    return this.words;
  }

  public void setWords(String words) {
    this.words = words;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getRemoteId() {
    return this.remoteId;
  }

  public void setRemoteId(Long remoteId) {
    this.remoteId = remoteId;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Generated(hash = 1292169202)
  public HouseModel(Long id, @NotNull Long remoteId, @NotNull String url,
      String words, @NotNull String name) {
    this.id = id;
    this.remoteId = remoteId;
    this.url = url;
    this.words = words;
    this.name = name;
  }

  @Generated(hash = 1408099660)
  public HouseModel() {
  }
}
