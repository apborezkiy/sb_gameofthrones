package data.storage.models;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import data.storage.models.CharacterModel;
import data.storage.models.HouseModel;

import data.storage.models.CharacterModelDao;
import data.storage.models.HouseModelDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig characterModelDaoConfig;
    private final DaoConfig houseModelDaoConfig;

    private final CharacterModelDao characterModelDao;
    private final HouseModelDao houseModelDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        characterModelDaoConfig = daoConfigMap.get(CharacterModelDao.class).clone();
        characterModelDaoConfig.initIdentityScope(type);

        houseModelDaoConfig = daoConfigMap.get(HouseModelDao.class).clone();
        houseModelDaoConfig.initIdentityScope(type);

        characterModelDao = new CharacterModelDao(characterModelDaoConfig, this);
        houseModelDao = new HouseModelDao(houseModelDaoConfig, this);

        registerDao(CharacterModel.class, characterModelDao);
        registerDao(HouseModel.class, houseModelDao);
    }
    
    public void clear() {
        characterModelDaoConfig.getIdentityScope().clear();
        houseModelDaoConfig.getIdentityScope().clear();
    }

    public CharacterModelDao getCharacterModelDao() {
        return characterModelDao;
    }

    public HouseModelDao getHouseModelDao() {
        return houseModelDao;
    }

}
