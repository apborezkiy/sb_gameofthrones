package data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import data.network.res.CharacterRes;

/**
 * Created by Энергичный Здоровый on 16.10.2016.
 */

@Entity(active = true, nameInDb = "CHARACTERS")
public class CharacterModel {

  @Id
  private Long id;

  @Unique
  @NotNull
  public Long remoteId;
  
  @NotNull
  private Long houseId;

  public String url;
  public String name;
  public String born;
  public String died;
  public String father;
  public String mother;
  public String aliases;
  public String titles;
  public String tvSeries;

  /** Used for active entity operations. */
  @Generated(hash = 1375777403)
  private transient CharacterModelDao myDao;

  /** Used to resolve relations */
  @Generated(hash = 2040040024)
  private transient DaoSession daoSession;

  public CharacterModel(Long remoteId, Long houseId, CharacterRes character) {
    this.remoteId = remoteId;
    this.houseId = houseId;
    this.url = character.url;
    this.name = character.name;
    this.born = character.born;
    this.died = character.died;
    this.father = character.father;
    this.mother = character.mother;
    this.titles = character.titles.toString();
    this.aliases = character.aliases.toString();
    this.tvSeries = character.tvSeries.toString();
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 1942392019)
  public void refresh() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 713229351)
  public void update() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 128553479)
  public void delete() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
  }

  /** called by internal mechanisms, do not call yourself. */
  @Generated(hash = 286595538)
  public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getCharacterModelDao() : null;
  }

  public String getMother() {
    return this.mother;
  }

  public void setMother(String mother) {
    this.mother = mother;
  }

  public String getFather() {
    return this.father;
  }

  public void setFather(String father) {
    this.father = father;
  }

  public String getDied() {
    return this.died;
  }

  public void setDied(String died) {
    this.died = died;
  }

  public String getBorn() {
    return this.born;
  }

  public void setBorn(String born) {
    this.born = born;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getHouseId() {
    return this.houseId;
  }

  public void setHouseId(Long houseId) {
    this.houseId = houseId;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getRemoteId() {
    return this.remoteId;
  }

  public void setRemoteId(Long remoteId) {
    this.remoteId = remoteId;
  }

  public String getAliases() {
    return this.aliases;
  }

  public void setAliases(String aliases) {
    this.aliases = aliases;
  }

  public String getTitles() {
    return this.titles;
  }

  public void setTitles(String titles) {
    this.titles = titles;
  }

  public String getTvSeries() {
    return this.tvSeries;
  }

  public void setTvSeries(String tvSeries) {
    this.tvSeries = tvSeries;
  }

  @Generated(hash = 1868847479)
  public CharacterModel(Long id, @NotNull Long remoteId, @NotNull Long houseId, String url,
      String name, String born, String died, String father, String mother, String aliases,
      String titles, String tvSeries) {
    this.id = id;
    this.remoteId = remoteId;
    this.houseId = houseId;
    this.url = url;
    this.name = name;
    this.born = born;
    this.died = died;
    this.father = father;
    this.mother = mother;
    this.aliases = aliases;
    this.titles = titles;
    this.tvSeries = tvSeries;
  }

  @Generated(hash = 360334686)
  public CharacterModel() {
  }
}
