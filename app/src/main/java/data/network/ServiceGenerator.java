package data.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by apborezkiy on 15.10.16.
 */

public class ServiceGenerator {
    private static String BASE_URL = "https://anapioficeandfire.com/api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder ();

    private static Retrofit.Builder sBuilder =
            new Retrofit.Builder()
            .baseUrl(ServiceGenerator.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static  <S> S createService(Class<S> serviceClass) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(logging);
        httpClient.addNetworkInterceptor(new StethoInterceptor());

        Retrofit retrofit = sBuilder
                .client(httpClient.build())
                .build();
        return retrofit.create(serviceClass);
    }
}
