package data.network;

import java.util.List;

import data.network.req.HouseReq;
import data.network.res.CharacterRes;
import data.network.res.HouseRes;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by apborezkiy on 15.10.16.
 */

public interface RestService {
    @GET("houses")
    Call<List<HouseRes>> getHouses ();

    @GET("characters/{id}")
    Call<CharacterRes> getCharacter (@Path("id") String id);
}
