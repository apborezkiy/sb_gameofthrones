package data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import data.network.RestService;
import data.network.ServiceGenerator;
import data.network.res.HouseRes;
import data.network.res.CharacterRes;
import data.storage.models.HouseModel;
import data.storage.models.CharacterModel;
import data.storage.models.HouseModelDao;
import data.storage.models.CharacterModelDao;
import data.storage.models.DaoSession;
import retrofit2.Call;
import retrofit2.Response;
import ui.activities.SplashActivity;
import utils.ConstantManager;
import utils.NetworkStatusChecker;
import utils.ThisApplication;

/**
 * Created by apborezkiy on 15.10.16.
 */

// TODO: 26.10.16 DataManager превратился практически в антипаттерн GodObject
public class DataManager implements ConstantManager {
    private static DataManager ourInstance = new DataManager();
    private static String TAG = "Data Manager";

    public static DataManager getInstance() {
        return ourInstance;
    }

    private Context mContext;
    private RestService mRestService;
    private DaoSession mDaoSession;
    private SharedPreferences mSharedPreferences;

    private List<HouseRes> houses = new ArrayList<>();
    private MultiMap<Long, CharacterRes> characters = new MultiValueMap<Long, CharacterRes>();

    public List<HouseModel> housesLocal;
    public MultiMap<Long, CharacterModel> charactersLocal = new MultiValueMap<Long, CharacterModel>();

    private DataManager() {
        mContext = ThisApplication.getContext();
        mRestService = ServiceGenerator.createService(RestService.class);
        mDaoSession = ThisApplication.getDaoSession();
        mSharedPreferences = ThisApplication.getSharedPreferences();
    }

    public Context getContent () {
        return mContext;
    }

    public Call<List<HouseRes>> getHouses() {
        return mRestService.getHouses();
    }

    public Call<CharacterRes> getCharacter(String remoteId) {
        return mRestService.getCharacter(remoteId);
    }
    
    public void saveToDatabase() {
        Boolean isFirstLaunch = mSharedPreferences.getBoolean(ConstantManager.IS_FIRST_LAUNCH, true);
        if (isFirstLaunch == true) {
            Log.d(TAG, "Saving to local database...");
            try {
                for (HouseRes house : houses) {
                    String[] parts = house.url.split("/");
                    Long remoteId = Long.valueOf(parts[parts.length - 1]);
                    Log.d(TAG, "Saving house to local database " + remoteId.toString());
                    mDaoSession.getHouseModelDao().insertOrReplace(new HouseModel(remoteId, house));
                    List<CharacterRes> _characters = (List<CharacterRes>) characters.get(remoteId);
                    if (_characters != null) {
                        for (CharacterRes character : _characters) {
                            String[] ch_parts = character.url.split("/");
                            Long remoteCharacterId = Long.valueOf(ch_parts[ch_parts.length - 1]);
                            Log.d(TAG, "Saving character to local database " + remoteCharacterId.toString() + " at house " + remoteId.toString());
                            mDaoSession.getCharacterModelDao().insertOrReplace(new CharacterModel(remoteCharacterId, remoteId, character));
                        }
                    } else {
                        Log.d(TAG, "no members in house " + remoteId);
                    }
                }

                SharedPreferences.Editor edit = mSharedPreferences.edit();
                edit.putBoolean(ConstantManager.IS_FIRST_LAUNCH, false);
                edit.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Boolean loadFromDatabaseOrRemoteService() throws CodeException, IOException {
        Boolean isFirstLaunch = mSharedPreferences.getBoolean(ConstantManager.IS_FIRST_LAUNCH, true);
        if (isFirstLaunch == true) {
            Log.d(TAG, "Loading from remote service...");
            if (NetworkStatusChecker.isNetworkAvailable(ThisApplication.getContext())) {
                Call<List<HouseRes>> callHouses = getHouses();
                Response<List<HouseRes>> resHouses = callHouses.execute();
                List<HouseRes> _houses = resHouses.body();
                if (resHouses.code() == 200) { // TODO: 26.10.16 Лучше проверять resHouses.isSuccessful(). Успешные запросы возвращают не только 200 
                    for (HouseRes house : _houses) {
                        houses.add(house);
                        String[] houses_parts = house.url.split("/"); // TODO: 26.10.16 Зачем нам целый  массив частей строки? Достаточно url.substring(url.lastIndexOf("/") + 1)
                        final String house_id = houses_parts[houses_parts.length - 1];
                        Integer left = house.swornMembers.size();
                        if (left > 0) {
                            String[] left_parts = house.swornMembers.get(left - 1).split("/");
                            if (left_parts.length > 0) {
                                final String left_id = left_parts[left_parts.length - 1];
                                for (Integer i = 0; i < left; ++i) {
                                    String[] parts = house.swornMembers.get(i).split("/");
                                    final String id = parts[parts.length - 1];
                                    Call<CharacterRes> callCharacter = getCharacter(id);
                                    Response<CharacterRes> resCharacter = callCharacter.execute();
                                    if (resCharacter.code() == 200) {
                                        CharacterRes character = resCharacter.body();
                                        Log.d(TAG, "Character loaded " + id.toString() + " at house " + house.name + " from server");
                                        characters.put(Long.valueOf(house_id), character);
                                    } else {
                                        throw new CodeException(resCharacter.code());
                                    }
                                }
                            }
                        }
                    }
                    saveToDatabase();
                } else {
                    throw new CodeException(resHouses.code());
                }
            } else {
                throw new CodeException(-1);
            }
        }

        Log.d(TAG, "Loading from local database...");
        housesLocal = getHousesLocal();
        for (HouseModel house : housesLocal) {
            Log.d(TAG, "House loaded from database: " + house.remoteId.toString());
            for (CharacterModel character : house.getSwornMembers()) {
                Log.d(TAG, "Character loaded from database: " + character.remoteId.toString());
                charactersLocal.put(house.remoteId, character);
            }
        }

        return true;
    }

    public List<HouseModel> getHousesLocal() {
        List<HouseModel> houses = null;
        try {
            houses = mDaoSession.queryBuilder(HouseModel.class)
                .orderAsc(HouseModelDao.Properties.Name)
                .build()
                .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return houses;
    }

    public CharacterModel getCharacterLocal(Long remoteId) {
        CharacterModel character = null;
        try {
            character = mDaoSession.queryBuilder(CharacterModel.class)
                .where(CharacterModelDao.Properties.RemoteId.eq(remoteId))
                .build().unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return character;
    }

    public List<String> getHousesNamesLocal() {
        List<String> names = new ArrayList<String>();
        for (HouseModel house : housesLocal) {
            names.add(house.name);
        }
        return names;
    }

    public List<CharacterModel> getCharacters(Long houseRemoteId) {
        List<CharacterModel> characters = (List<CharacterModel>) charactersLocal.get(houseRemoteId);
        if (characters == null) {
            characters = new ArrayList<>();
            Log.d(TAG, "No members in house " + houseRemoteId);
        }
        return characters;
    }

    public HouseModel getHouseLocal(Long houseRemoteId) {
        for (HouseModel house : housesLocal) {
            if (house.remoteId == houseRemoteId)
                return house;
        }
        return null;
    }

    public class CodeException extends Exception {
        private Integer mCode;

        public CodeException (Integer code) {
            mCode = code;
        }

        public Integer code() {
            return mCode;
        }
    }
}
